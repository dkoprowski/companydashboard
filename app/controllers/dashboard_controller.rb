class DashboardController < ApplicationController
    def index
        @posts = Post.all.order("created_at desc")
        @paginatedPost = Post.all.order("created_at desc").paginate(page: params[:page], per_page: 5)
    end
    
    def mainboard
        @userPosts = Post.where(user_id: params[:user_id]).order("created_at desc").paginate(page: params[:page], per_page: 5)
    end
end
