Koprowski Daniel [215572](https://evening-stream-3082.herokuapp.com).[Company Dashboard](https://bitbucket.org/KOP3R/companydashboard).  
  
README  
  
Projekt na zaliczenie Architektura Serwisów Komputerowych  
* Ruby version: 2.2.1  
* Ruby on Rails version: 4.2.1  
* Dodatkowe gemy:  
gem 'sass-rails',           '4.0.5'  
gem 'bootstrap-sass',       '3.2.0.0'  
gem 'will_paginate',        '~> 3.0.6'  
gem 'bcrypt',               '3.1.7'  
  
* Funkcjonalności:  
Czat  
Dodawanie postów  
Usuwanie postów (tylko swoich)  
Wyświetlanie postów **po kliknięciu w nick** przy wiadomości  
Rejestracja  
Logowanie  
Paginacja  
* Dodatkowe uwagi:  
Nie udało mi się stworzyć pokojów czatu więc na razie wszystko jest w jednym streamie  
Aby móc dodawać, usuwać posty trzeba się zarejestrować/zalogować  

* Aplikacja na Heroku:  
https://evening-stream-3082.herokuapp.com